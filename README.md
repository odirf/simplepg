# Simple password generator


**simplepg is a *simple* password generator written in Python, with which you can generate passwords.**

---
## Installing and Usage

- Clone the repository

- Navigate to the cloned repository

- Run the generator (`main.py`):
```python3 main.py```

You will be asked how long the password should be, and the password will be generated.

---

### To-Do/Ideas:

- Custom characters in password
- auto copy-to-clipboard
- 
